package controllers;

import models.dbProducto;
import models.Productos;
import views.dlgVista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.sql.*;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.TableModel;

public class Controlador implements ActionListener  {
    
    private dlgVista Vista;
    private Productos Pro;
    private dbProducto dbPro;
    boolean act;
    
    public Controlador(dlgVista Vista, Productos Pro, dbProducto dbPro){
        this.Vista=Vista;
        this.Pro=Pro;
        this.dbPro=dbPro;
        
        Vista.btnNuevo.addActionListener(this);
        Vista.btnGuardar.addActionListener(this);
        Vista.btnLimpiar.addActionListener(this);
        Vista.btnDeshabilitar.addActionListener(this);
        Vista.btnCerrar.addActionListener(this);
        Vista.btnLimpiar.addActionListener(this);
        Vista.btnBuscar.addActionListener(this);
        Vista.btnBuscarDesactivar.addActionListener(this);
        Vista.btnHabilitar.addActionListener(this);
    }
    private void iniciarVista() throws Exception{
        mosTabla();
        //dbPro.listar();
        Vista.setTitle("Datos");
        Vista.setSize(800,800);
        Vista.setVisible(true);
    } 
    public void mosTabla(){
        try {
            Vista.jtbActivos.setModel((TableModel) dbPro.listar());
            Vista.jtbNoActivos.setModel(dbPro.listarD());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(Vista, "no se pudo mostrar la tabla "+e.getMessage());
        }
    }
    
    public void fecha(){
        int mes = Vista.jdtFecha.getCalendar().get(Calendar.MONTH) + 1;
        int dia = Vista.jdtFecha.getCalendar().get(Calendar.DAY_OF_MONTH);
        int año = Vista.jdtFecha.getCalendar().get(Calendar.YEAR);
        String fecha;
        if (mes < 10) {
            fecha = String.format("%d,0%d,%d", año, mes, dia);
        } else {
            fecha = String.format("%d,%d,%d", año, mes, dia);
        }
        Pro.setFecha(fecha);
    }

    public void Limpiar(){

       Vista.txtCodigo.setText("");
       Vista.txtNombre.setText("");
       Vista.txtPrecio.setText("");
       Vista.jdtFecha.setDate(null);
       Vista.txtCodigoDesactivado.setText("");
       Vista.txtNombreDesactivado.setText("");
 
    }
    public void LimpiarDes(){

       Vista.txtCodigoDesactivado.setText("");
       Vista.txtNombreDesactivado.setText("");
       
       
    }
    public boolean isVacio(){
        if(Vista.txtCodigo.getText().isEmpty() ||Vista.txtNombre.getText().isEmpty()|| Vista.txtPrecio.getText().isEmpty()){
            return true;
        }
        else{
            return false;
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==Vista.btnNuevo){
            
            //Vista.txtId.setEnabled(true);
            Vista.txtCodigo.setEnabled(true);
            Vista.txtNombre.setEnabled(true);
            Vista.txtPrecio.setEnabled(true);
            Vista.jdtFecha.setEnabled(true);
            Vista.jtbActivos.setEnabled(true);
            
            
            Vista.btnCancelar.setEnabled(true);
            Vista.btnLimpiar.setEnabled(true);
            Vista.btnGuardar.setEnabled(true);
            Vista.btnNuevo.setEnabled(true);
            Vista.btnCerrar.setEnabled(true);
            Vista.btnBuscar.setEnabled(true);
            Vista.btnDeshabilitar.setEnabled(true);
        }
        if(e.getSource()==Vista.btnGuardar){
            try {
                if(dbPro.isExiste(Vista.txtCodigo.getText())){
                    Pro.setCodigo(Vista.txtCodigo.getText()); 
                    Pro.setNombre(Vista.txtNombre.getText());
                    Pro.setPrecio(Float.parseFloat(Vista.txtPrecio.getText()));
                    Pro.setStatus(1);
                    fecha();
                    try {
                        dbPro.insertar(Pro);
                        //dbPro.actualizar(Pro);
                        JOptionPane.showMessageDialog(Vista, "Se guardo con exito");

                    } catch (Exception e1) {
                        JOptionPane.showMessageDialog(Vista,  "Surgio el siguiente error: "+e1.getMessage());
                    }
                }else{
                    Pro.setCodigo(Vista.txtCodigo.getText()); 
                    Pro.setNombre(Vista.txtNombre.getText());
                    Pro.setPrecio(Float.parseFloat(Vista.txtPrecio.getText()));
                    Pro.setStatus(0);
                    fecha();
                    dbPro.actualizar(Pro);
                    //JOptionPane.showMessageDialog(Vista, "Ingresa un codigo que no se repita");
                }

            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(Vista, "No se pudo guardar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(Vista, "No se pudo guardar, surgio el siguiente error: "+ex2.getMessage());
            }
            mosTabla();

            
        }
        
        if(e.getSource()==Vista.btnBuscar){
            
            try{
                Pro = (Productos) dbPro.buscar(Vista.txtCodigo.getText());
                Pro.setCodigo(Vista.txtCodigo.getText());
                Limpiar();
                dbPro.buscar(Vista.txtCodigo.getText());
                Vista.txtCodigo.setText(Pro.getCodigo());
                Vista.txtPrecio.setText(String.valueOf(Pro.getPrecio())); 
                Vista.txtNombre.setText(Pro.getNombre()); 
                if(dbPro.buscar(Vista.txtCodigo.getText()).equals(-1)){
                    JOptionPane.showMessageDialog(Vista, "No se encontro");
                }
                else{
                    JOptionPane.showMessageDialog(Vista, "Se encontro");
                    Vista.txtCodigo.setText(Pro.getCodigo());
                   Vista.txtPrecio.setText(String.valueOf(Pro.getPrecio())); 
                   Vista.txtNombre.setText(Pro.getNombre());
                   Vista.jdtFecha.setDate(Date.valueOf(Pro.getFecha()));
                }
                
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(Vista, "No se pudo Buscar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(Vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
            
        }
        if(e.getSource()==Vista.btnBuscarDesactivar){
            try{
                Pro = (Productos) dbPro.buscarD(Vista.txtCodigoDesactivado.getText());
                Pro.setCodigo(Vista.txtCodigoDesactivado.getText());
                Limpiar();
                dbPro.buscar(Vista.txtCodigoDesactivado.getText());
                Vista.txtCodigoDesactivado.setText(Pro.getCodigo()); 
                Vista.txtNombreDesactivado.setText(Pro.getNombre());
                
                if(dbPro.buscarD(Vista.txtNombreDesactivado.getText()).equals(-1)){
                    JOptionPane.showMessageDialog(Vista, "Se encontro");
                }
                else
                    JOptionPane.showMessageDialog(Vista, "No Se encontro");
                
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(Vista, "No se pudo Buscar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(Vista, "Surgio el siguiente error: "+ex2.getMessage());
            }
            
        }
        if(e.getSource()==Vista.btnCerrar){
        int option=JOptionPane.showConfirmDialog(Vista,"¿Deseas salir?",
        "Decide", JOptionPane.YES_NO_OPTION);
         if(option==JOptionPane.YES_NO_OPTION){
             Vista.dispose();
             System.exit(0);
            }
           
        }
        if(e.getSource()==Vista.btnDeshabilitar){
            try {
                if(dbPro.deshabilitar(Pro)){
                    dbPro.actualizar(Pro);
                    JOptionPane.showMessageDialog(Vista, "Se deshabilito con exito");
                }
                else{
                    JOptionPane.showMessageDialog(Vista, "No se pudo deshabilitar");
                }
                mosTabla();
             }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(Vista, "No se pudo guardar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(Vista, "No se pudo guardar, surgio el siguiente error: "+ex2.getMessage());
            }
            Limpiar();
        } 
        if(e.getSource()==Vista.btnHabilitar){
            try {
                if(dbPro.habilitar(Pro)){
                    dbPro.actualizar(Pro);
                    JOptionPane.showMessageDialog(Vista, "Se habilito con exito");
                }
                else{
                    JOptionPane.showMessageDialog(Vista, "No se pudo habilitar");
                }
                mosTabla();
             }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(Vista, "No se pudo guardar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(Vista, "No se pudo guardar, surgio el siguiente error: "+ex2.getMessage());
            }
            Limpiar();
        }
        
            
        if(e.getSource()==Vista.btnCancelar){
            Limpiar();
           // Vista.txtId.setEnabled(true);
            Vista.txtCodigo.setEnabled(true);
            Vista.txtNombre.setEnabled(true);
            Vista.txtPrecio.setEnabled(true);
            Vista.jdtFecha.setEnabled(true);
            
            Vista.btnCancelar.setEnabled(true);
            Vista.btnLimpiar.setEnabled(true);
            Vista.btnGuardar.setEnabled(true);
            Vista.btnNuevo.setEnabled(true);
            
            
        }
        if(e.getSource()==Vista.btnLimpiar){
            Limpiar();
        }
        
      
        }
        public static void main(String[] args) throws Exception {
        Productos pro=new Productos();
        dbProducto dbPro=new dbProducto();
        dlgVista vista=new dlgVista(new JFrame(),true);
        Controlador contro=new Controlador(vista,pro,dbPro);
        contro.iniciarVista();
                
    }
    }
    
    
    
    
    
    

